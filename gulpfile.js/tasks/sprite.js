var config      = require('../config')
if(!config.tasks.images) return

var browserSync = require('browser-sync')
var changed     = require('gulp-changed')
var gulp        = require('gulp')
var imagemin    = require('gulp-imagemin')
var path        = require('path')
var spritesmith = require("gulp.spritesmith")
// var merge       = require('merge-stream')
// var gulpif      = require('gulp-if')

var paths = {
  src: path.join(config.root.src, config.tasks.images.src, '/sprite/*.png'),
  dest: path.join(config.root.dest, config.tasks.images.dest, '/'),
  css: path.join(config.root.src, config.tasks.css.src, '/modules/')
}

var spriteTask = function() {
  var spriteData = gulp.src(paths.src).pipe(spritesmith({
    imgName: '../images/sprite.png',
    cssName: '_sprite.scss'
  }));

  spriteData.img.pipe(gulp.dest(paths.dest));
  spriteData.css.pipe(gulp.dest(paths.css));
}

gulp.task('sprite', spriteTask)
module.exports = spriteTask